<h1>Getting started</h1>

<h2>Setup</h2>

<p>Clone/pull/download this repository</p>
<p>Create a virtualenv with pipenv, if you don't have it you can install by running pip install pipenv. Then install dependencies with:</p>

### `pipenv install`

<h2>Run app</h2>

<p>Open your command line in the root directory and open the env shell:</p>

### `pipenv shell`

<p>Now you can start the app</p>

### `python manage.py runserver`
