# Generated by Django 2.2.14 on 2020-07-31 14:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('file', '0005_auto_20200731_1447'),
    ]

    operations = [
        migrations.AddField(
            model_name='file',
            name='basename',
            field=models.CharField(default='test.jpg', max_length=255),
            preserve_default=False,
        ),
    ]
