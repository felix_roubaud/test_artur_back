from django.urls import include, path
from rest_framework import routers
from .views import FileView

app_name = 'file'
router = routers.SimpleRouter()
router.register(
    r'file',
    FileView,
    basename='file',
)

urlpatterns = [
    path('', include(router.urls)),
]