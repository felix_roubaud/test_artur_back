from django.db import models
from django.conf import settings
from user.models import User
# Create your models here.


class File(models.Model):
    title = models.CharField(max_length=255, null=False)
    basename = models.CharField(max_length=255, null=False)
    description = models.CharField(max_length=255, null=True,blank=True)
    document = models.FileField(upload_to='documents',blank=True, null=True)
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
    )