import os
from django.shortcuts import render
from rest_framework import viewsets
from .serializers import FileSerializer
from rest_framework.permissions import IsAuthenticated
from .models import File
from django.http import HttpResponse
from django.conf import settings
from wsgiref.util import FileWrapper
from django.http import FileResponse


class FileView(viewsets.ModelViewSet):
    serializer_class = FileSerializer
    permission_classes = (IsAuthenticated,)
    pagination_class = None

    # Provide access to files only to authenticated user files
    def get_queryset(self):
        return File.objects.filter(user__id=self.request.user.id)

    def create(self, request):
        user = request.user
        document =request.FILES.get('file',None)
        if document:
            request.data["document"]=document
            request.data["title"]=document =request.data.get('title',None)
            request.data["basename"]=document =request.data.get('baseName',None)
            request.data["description"]=document =request.data.get('description',None)
        return super().create(request)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        file_name = os.path.basename(instance.document.url)   
        res = FileResponse(open(os.path.join(settings.MEDIA_ROOT,instance.document.name),"rb"), content_type='png')
        res['Content-Disposition'] = 'attachment; filename="%s"' % instance.document.name
        return res