from django.urls import reverse
from rest_framework.test import APITestCase, APIClient
from rest_framework.views import status
from .models import File
from .serializers import FileSerializer
from user.models import User
from rest_framework.authtoken.models import Token


class BaseViewTest(APITestCase):
    client = APIClient()

    @staticmethod
    def create_file(user,title="", description=""):
        if title != "" and description != "" and user:
            File.objects.create(title=title, description=description,user=user)

    def setUp(self):
        # add test data
        self.user = User.objects.create_user(username='testUser',password='123456')
        self.token = Token.objects.create(user=self.user)
        self.create_file(self.user,"passport", "My passport")
        self.create_file(self.user,"ID", "My ID")

class FilesTest(BaseViewTest):

    def test_get_all_files(self):
        # Test that all files are effectively returned when we hit the file endpoint
        self.client.force_login(user=self.user)
        response = self.client.get(
            reverse("file:file-list"),HTTP_AUTHORIZATION='Token {}'.format(self.token)
        )
        dbFiles = File.objects.all()
        serialized = FileSerializer(dbFiles, many=True)
        self.assertEqual(response.data, serialized.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_check_file_protection(self):
        # Test that a user can only access its own files
        user2 = User.objects.create_user(username='testUser2',password='123456')
        self.create_file(user2,"passport2", "My passport2")
        self.client.force_login(user=self.user)
        response = self.client.get(
            reverse("file:file-list"),HTTP_AUTHORIZATION='Token {}'.format(self.token)
        )
        dbFiles = File.objects.all()
        serialized = FileSerializer(dbFiles, many=True)
        self.assertEqual(len(response.data),2)
        self.assertEqual(len(serialized.data),3)
