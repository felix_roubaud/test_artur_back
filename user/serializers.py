from djoser.serializers import UserCreateSerializer as BaseUserRegistrationSerializer
from .models import User
from rest_framework import serializers

class UserRegistrationSerializer(BaseUserRegistrationSerializer):
    class Meta(BaseUserRegistrationSerializer.Meta):
        fields = ( 'id', 'email', 'username','first_name', 'last_name', 'photo', 'password', )
    # def create(self, validated_data):
    #      user = User.objects.create_user(**validated_data)
    #      user.save()
    #      return user


class UserSerializer(serializers.ModelSerializer):
    photo = serializers.SerializerMethodField()
    class Meta:
        model = User
        fields = ( 'id', 'email', 'username','first_name', 'last_name', 'photo' ,'friends')
    
    # need to explicit this field because search pagination uses base fileField serialization differently
    def get_photo(self, obj):
        if obj.photo:
            return obj.photo.url
        else:
            return None

class UserPhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ( 'id', 'photo')