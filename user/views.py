import os
from rest_framework import viewsets
from .serializers import UserSerializer, UserPhotoSerializer
from rest_framework.permissions import IsAuthenticated
from .models import User
from rest_framework.decorators import action
from rest_framework import filters, status
from rest_framework.response import Response
from django.conf import settings

class UserView(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all().exclude(is_superuser=True)
    permission_classes = (IsAuthenticated,)
    filter_backends = [filters.SearchFilter]
    search_fields = ['username','email','last_name','first_name']

    # paginate search queryset which might be too heavy
    def paginate_queryset(self, queryset):
        if self.paginator and self.request.query_params.get("search", None) is None:
            return None
        return super().paginate_queryset(queryset)

    def partial_update(self, request, *args, **kwargs):
        serializer = self.serializer_class(request.user, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(detail=False, methods=['post'])
    def add_friends(self, request):
        try:
            request.user.add_friends(request.data['users_id'])
            return Response(status=status.HTTP_200_OK)
        except:
            return Response( status=status.HTTP_400_BAD_REQUEST)

    
    @action(detail=False, methods=['post'])
    def remove_friend(self, request):
        try:
            request.user.remove_friend(request.data['user_id'])
            return Response( status=status.HTTP_200_OK)
        except:
            return Response( status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['get'])
    def get_friends(self, request):
        try:
            serialized_friends = self.serializer_class(request.user.friends.all(),many=True).data
            return Response(serialized_friends, status=status.HTTP_200_OK)
        except:
            return Response( status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['get'])
    def get_me(self, request):
        try:
            me = self.serializer_class(request.user).data
            return Response(me, status=status.HTTP_200_OK)
        except:
            return Response( status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False,methods=['patch'])
    def photo_upload(self, request):
        photo = request.FILES.get('file')
        photo.name = '{}.{}'.format(request.user.id,photo.name)
        
        # UserSerializer needs custom photo field so use specifric serializer
        serialized = UserPhotoSerializer(request.user,data={'photo':photo},partial=True)
        if serialized.is_valid():
            previous_photo = request.user.photo
            try:
                os.remove(previous_photo.path)
            except:
                pass
            serialized.save()
            return Response( status=status.HTTP_200_OK)
        return Response( status=status.HTTP_400_BAD_REQUEST)




