from django.urls import reverse
from rest_framework.test import APITestCase, APIClient
from rest_framework.views import status
from .models import User
from .serializers import UserSerializer
from rest_framework.authtoken.models import Token


class BaseViewTest(APITestCase):
    client = APIClient()

    def setUp(self):
        # add test data
        self.user = User.objects.create_user(username='testUser',password='123456')
        self.user2 = User.objects.create_user(username='testUser2',password='123456')
        self.user3 = User.objects.create_user(username='testUser3',password='123456')

        self.token = Token.objects.create(user=self.user)

class FriendsTest(BaseViewTest):

    def test_add_friend(self):
        self.client.force_login(user=self.user)
        self.client.post(
           'http://localhost:8000/api/v1/user/add_friends/',{'users_id':[self.user2.id,self.user3.id]},HTTP_AUTHORIZATION='Token {}'.format(self.token)
        )
        response = self.client.get(
            'http://localhost:8000/api/v1/user/get_friends/',HTTP_AUTHORIZATION='Token {}'.format(self.token)
        )
        self.user.refresh_from_db()
        friends= self.user.friends.all()
        serialized = UserSerializer(friends, many=True)
        self.assertEqual(response.data, serialized.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
