from django.urls import include, path
from rest_framework import routers
from .views import UserView

app_name = 'user'
router = routers.SimpleRouter()
router.register(
    r'^user',
    UserView,
    basename='user',
)


urlpatterns = [
    path('', include(router.urls)),
]