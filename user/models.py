from django.contrib.auth.models import AbstractUser
from django.db import models

class User(AbstractUser):
    photo = models.ImageField(upload_to='profile_picture',blank=True, null=True)
    friends = models.ManyToManyField("self", blank=True,symmetrical=False)

    def add_friends(self,friends_id):
        for id in friends_id:
            if id != self.id:
                new_friend = User.objects.get(id=id)
                self.friends.add(new_friend)
    
    def remove_friend(self,friend_id):
        friend = User.objects.get(id=friend_id)
        self.friends.remove(friend)